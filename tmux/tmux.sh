#!/bin/bash

#
# This is a bash helper function which updates the environment of an existing
# tmux session.
#
# For example: The tmux session runs on an other computer and you are connected
# via ssh and use X forwarding. After detaching the session and closing the ssh
# connection the DISPLAY variable may be changed next time you login via ssh.
# But your tmux session keeps still the old DISPLAY variable after attaching.
# Use this function to update all tmux environment variables for the running
# session.
#
# This function is a simple wrapper in order to add new commands to tmux. Call
# the tmux-function with one of the following arguments:
#   $ tmux update-environment
#   $ tmux update-env
#   $ tmux env-update
#
# If you call the tmux-function with any other commands the tmux will be
# executed instead and the arguments will be parsed to it.
#
# Source this file in your .bashrc
#
# copied from: https://raimue.blog/2013/01/30/tmux-update-environment/
#
function tmux() {
    local tmux=$(type -fp tmux)
    case "$1" in
        update-environment|update-env|env-update)
            local v
            while read v; do
                if [[ $v == -* ]]; then
                    unset ${v/#-/}
                else
                    # Add quotes around the argument
                    v=${v/=/=\"}
                    v=${v/%/\"}
                    eval export $v
                fi
            done < <(tmux show-environment)
            ;;
        *)
            $tmux "$@"
            ;;
    esac
}
