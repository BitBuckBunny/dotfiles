augroup error_formats

    " clear all auto commands for this group
    autocmd!

    """""""""""""""""""""""""""""""""""""""""""""""""""""""
    " VHDL
    """""""""""""""""""""""""""""""""""""""""""""""""""""""

    " clear
    autocmd FileType vhdl set errorformat=

    " set directory stack tracing.
    autocmd FileType vhdl set errorformat+=%Dmake:\ Entering\ directory\ %[`']%f'
    autocmd FileType vhdl set errorformat+=%Xmake:\ Leaving\ directory\ %[`']%f'
    " call make in make
    autocmd FileType vhdl set errorformat+=%Dmake[%\\d%#]:\ Entering\ directory\ %[`']%f'
    autocmd FileType vhdl set errorformat+=%Xmake[%\\d%#]:\ Leaving\ directory\ %[`']%f'

    "
    " ModelSim / QuestaSim
    "
    " if a transcript file should be parsed each lines starts with string "# "
    " followed by the output of vcom/vlog/vlib ...
    " Therefore these two characters are optional for the errorformat
    "

    " vcom: file not found
    "autocmd FileType vhdl set errorformat+=%E%>#%\\?\ %\\?**\ %trror:\ (vcom-7)\ %m\ \"%f\"%.%#,
    "                                      \%Z%m

    " vlog: file not found
    "autocmd FileType vhdl set errorformat+=%E%>#%\\?\ %\\?**\ %trror:\ (vlog-7)\ %m\ \"%f\"%.%#,
    "                                      \%Z%m

    " vlib: can't create library
    "autocmd FileType vhdl set errorformat+=#%\\?\ %\\?**\ %trror:\ (vlib-35)\ %m

    " generic Error
    autocmd FileType vhdl set errorformat+=#%\\?\ %\\?**\ %trror:\ %f(%l):\ %m
    autocmd FileType vhdl set errorformat+=#%\\?\ %\\?**\ %trror:\ (%\\h%#-%\\d%#)\ %m

    " generic Warning
    autocmd FileType vhdl set errorformat+=#%\\?\ %\\?**\ %tarning:\ %f(%l):\ %m
    autocmd FileType vhdl set errorformat+=#%\\?\ %\\?**\ %tarning:\ (%\\h%#-%\\d%#)\ %m


    "
    " VCS-MX
    "

    " vlogan: file not found
    autocmd FileType vhdl set errorformat+=%E%>%trror-[SFCOR]\ Source\ file\ cannot\ be\ opened,
                                          \%C%m,
                                          \%C\"%f\",
                                          \%C%m,
                                          \%Z%m
    " vhdlan: file not found
    autocmd FileType vhdl set errorformat+=%E%>%trror:\ analysis\ Parsing\ vhdl-783,
                                          \%C%m,
                                          \%Z%m,
    " generic Error (vlogan/vhdlan)
    "autocmd FileType vhdl set errorformat+=%E%>%trror-[%[0-9a-zA-Z_-]%#]\ %m,
    autocmd FileType vhdl set errorformat+=%E%>%trror-[%\\S%#]\ %m,
                                          \%Z%f\\,\ %l
    " generic Warning
    "autocmd FileType vhdl set errorformat+=%W%>%tarning-[%[0-9a-zA-Z_-]%#]\ %m,
    autocmd FileType vhdl set errorformat+=%W%>%tarning-[%\\S%#]\ %m,
                                          \%Z%f\\,\ %l

    """""""""""""""""""""""""""""""""""""""""""""""""""""""

augroup END

